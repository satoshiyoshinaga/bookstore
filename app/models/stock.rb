class Stock < ActiveRecord::Base
  belongs_to :store
  belongs_to :book

  scope :in_store, ->(store) { where(store_id: store.id) }
end
