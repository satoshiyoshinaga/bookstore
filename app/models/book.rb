class Book < ActiveRecord::Base
  belongs_to :author
  has_many :stock

  scope :order_by_author_name, -> {
    joins(:author).order('authors.name ASC, books.id ASC')
  }

  scope :order_by_stock_quantity, -> {
    joins('LEFT OUTER JOIN stocks ON stocks.book_id = books.id')
      .group('books.id')
      .select('books.id, books.title, SUM(stocks.quantity) as sum_quantity')
      .order('sum_quantity DESC, books.id ASC')
    }
end
