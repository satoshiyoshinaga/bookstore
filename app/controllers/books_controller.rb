class BooksController < ApplicationController
  def index
    @books = Book.order_by_author_name
  end

  def with_quantity
    @books = Book.order_by_stock_quantity
  end
end
