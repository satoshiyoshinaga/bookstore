class StoresController < ApplicationController

  add_breadcrumb 'shops', :stores_path

  def index
    @stores = Store.all
  end

  def show
    @store = Store.find(params[:id])
    @stocks = Stock.in_store(@store)
      .joins(:book).order('books.title ASC')
      .includes(book: :author)
  end
end
