Rails.application.routes.draw do
  root 'books#index'
  resources :books, only: ['index'] do
    collection do
      get 'with_quantity'
    end
  end
  resources :stores, only: ['index', 'show']
end
