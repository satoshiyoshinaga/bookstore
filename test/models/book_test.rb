require 'test_helper'

class BookTest < ActiveSupport::TestCase

  test 'scope order_by_author_name' do
    rows = Book.order_by_author_name.pluck('authors.name')
    assert rows == rows.sort
  end

  test 'scope order_by_stock_quantity' do
    rows = Book.order_by_stock_quantity.map{ |row| row.sum_quantity || 0 }
    assert rows == rows.sort{ |a, b| b <=> a }
  end

end
