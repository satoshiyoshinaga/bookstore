class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :name, index: true

      t.timestamps
    end
  end
end
